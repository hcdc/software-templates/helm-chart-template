setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create configmap" {
    create_template '{"include_configmap": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/configmap.yaml" ]
}

@test "skip configmap" {
    create_template '{"include_configmap": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/configmap.yaml" ]
}