setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create buildconfig" {
    create_template '{"include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/buildconfig.yaml" ]
    assert [ -e "${TEMPLATE_FOLDER}/deployment.yaml" ]
    assert [ -e "${TEMPLATE_FOLDER}/target-buildconfigs.yaml" ]
    assert [ -e "${TEMPLATE_FOLDER}/target-secrets.yaml" ]
}

@test "skip buildconfig" {
    create_template '{"include_build_config": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/buildconfig.yaml" ]
    assert [ ! -e "${TEMPLATE_FOLDER}/target-buildconfigs.yaml" ]
    assert [ ! -e "${TEMPLATE_FOLDER}/target-secrets.yaml" ]
}

@test "create build secret" {
    create_template '{"include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/buildconfig.yaml" ]
    lint_chart "buildSecrets: {SOMETHING: null}"
}

@test "include build test stage" {
    create_template '{"include_build_config": "yes", "include_build_test_stage": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/buildconfig.yaml" ]
}

@test "override buildSource" {
    create_template '{"include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/buildconfig.yaml" ]
    lint_chart "buildSource: {gitlabProject: somerepo}"
}

@test "buildtargets" {
    create_template '{"include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/buildconfig.yaml" ]
    lint_chart "buildTargets: {some-project: {cluster: dev-cluster}}"
}