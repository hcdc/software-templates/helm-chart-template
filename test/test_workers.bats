setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create worker deploymentconfig" {
    create_template '{"include_workers": "yes", "include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/worker-deployments.yaml" ]
}

@test "create worker deployments" {
    create_template '{"include_workers": "yes", "include_build_config": "no"}'
    assert [ -e "${TEMPLATE_FOLDER}/worker-deployments.yaml" ]
}

@test "create worker without deployments" {
    create_template '{"include_workers": "yes", "include_deployment": "no", "include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/worker-deployments.yaml" ]
    assert [ ! -e "${TEMPLATE_FOLDER}/deployment.yaml" ]
}

@test "skip workers" {
    create_template '{"include_workers": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/worker-deployments.yaml" ]
}
