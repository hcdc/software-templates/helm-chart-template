# lint chart
# ==========
#
# Summary: Lint the chart and test if everything is ok
#
# Usage: lint_chart <config>

# Options:
#   <config>    The JSON-encoded configuration that you want to lint
#
# IO:
#   STDERR - the failed expression, on failure
# Globals:
#   none
# Returns:
#   0 - if the template has been created successfully
#   1 - otherwise
#
#   ```bash
#   @test 'lint()' {
#     lint_chart '{"some_param": "test"}'
#   }
#   ```
#
lint_chart() {
  echo "$1" >> "${PROJECT_FOLDER}/linter_values.yaml"
  helm lint -f ${PROJECT_FOLDER}/linter_values.yaml ${PROJECT_FOLDER}
}
