setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create cronjobs" {
    create_template '{"include_cronjobs": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/cronjobs.yaml" ]
}

@test "create cronjobs with volumes" {
    create_template '{"include_cronjobs": "yes", "include_volume": "one"}'
    assert [ -e "${TEMPLATE_FOLDER}/cronjobs.yaml" ]
}

@test "skip cronjobs" {
    create_template '{"include_cronjobs": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/cronjobs.yaml" ]
}