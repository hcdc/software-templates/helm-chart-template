setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create route" {
    create_template '{"include_route": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/routes.yaml" ]
}

@test "skip route" {
    create_template '{"include_route": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/routes.yaml" ]
}