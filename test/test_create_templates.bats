setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "skip templates" {
    create_template '{"create_templates": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/*.yaml" ]
}
