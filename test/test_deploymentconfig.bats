setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create deploymentconfig" {
    create_template '{"include_deployment": "yes", "include_build_config": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/deployment.yaml" ]
}

@test "create deployment" {
    create_template '{"include_deployment": "yes", "include_build_config": "no"}'
    assert [ -e "${TEMPLATE_FOLDER}/deployment.yaml" ]
}

@test "skip deployment and build config" {
    create_template '{"include_deployment": "no", "include_build_config": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/deployment.yaml" ]
}

@test "skip deploymentconfig" {
    create_template '{"include_deployment": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/deployment.yaml" ]
}