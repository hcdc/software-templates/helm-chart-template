setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create one volume" {
    create_template '{"include_volume": "one"}'
    assert [ -e "${TEMPLATE_FOLDER}/persistentvolumeclaims.yaml" ]
}

@test "create many volumes" {
    create_template '{"include_volume": "many"}'
    assert [ -e "${TEMPLATE_FOLDER}/persistentvolumeclaims.yaml" ]
}

@test "skip volumes" {
    create_template '{"include_volume": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/persistentvolumeclaims.yaml" ]
}