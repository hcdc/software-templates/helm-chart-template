setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create secret" {
    create_template '{"include_secret": "yes"}'
    assert [ -e "${TEMPLATE_FOLDER}/secret.yaml" ]
}

@test "skip secret" {
    create_template '{"include_secret": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/secret.yaml" ]
}