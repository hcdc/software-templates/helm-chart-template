setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "create service" {
    create_template '{"include_service": "yes", "service_port": "8080"}'
    assert [ -e "${TEMPLATE_FOLDER}/service.yaml" ]
}

@test "skip service" {
    create_template '{"include_service": "no"}'
    assert [ ! -e "${TEMPLATE_FOLDER}/service.yaml" ]
}