.. documentation on the parameters from cookiecutter.json

.. list-table:: Parameters
   :header-rows: 1

   * - Parameter
     - Description
     - Default
     - Format
   * - ``project_authors``
     - Full names of the author(s)
     - ``Firstname Lastname, Another-Firstname Lastname``
     - Full name in the format ``Firstname Lastname``.
       In case of multiple authors, they need to be comma-separated.

       .. important::

          The number of authors must match the number of emails in
          ``project_author_emails``!

   * - ``project_author_emails``
     - Email address of the author(s)
     - ``someone@hereon.de, someone.else@hereon.de``
     - Raw email as ``username@domain.de``.
       In case of multiple authors, they need to be comma-separated.

       .. important::

          The number of emails must match the number of authors in
          ``project_authors``

   * - ``project_maintainers``
     - Full names of the maintainer(s)
     - Fallback to ``project_authors``
     - Same as for ``project_authors``.

       .. important::
          The number of maintainers must match the number of emails in
          ``project_maintainer_emails``!
   * - ``project_maintainer_emails``
     - Email address of the maintainer(s)
     - Fallback to ``project_author_emails``
     - Same as for ``project_authors``.

       .. important::

          The number of emails must match the number of maintainers in
          ``project_maintainer_emails``!
   * - ``gitlab_host``
     - Fully qualified domain of the Gitlab Server.
     - ``codebase.helmholtz.cloud``
     - A valid domain name
   * - ``gitlab_username``
     - The user or group name on GitLab. Can also be the full path to a subgroup
     - ``"hcdc/kubernetes/charts"``
     - a single slug or multiple slugs separated by ``/``
   * - ``git_remote_protocoll``
     - Whether to register the remote as ``https`` or ``ssh`` after setting up
       the project locally. Does not have an effect on the documentation.
     - ``https``
     - One of ``https`` or ``ssh``
   * - ``institution``
     - Name of the institution that should be acknowledged
     - ``Helmholtz-Zentrum Hereon``
     - string
   * - ``institution_url``
     - Public website of the institution
     - ``https://www.hereon.de``
     - A url
   * - ``copyright_holder``
     - Body that has the copyright on the package.
     - Fallback to ``institution``
     - string
   * - ``copyright_year``
     - Year or year range of the copyright
     - the current year
     - a year (e.g. ``2013``) or a year range (e.g. ``2021-2023``)
   * - ``use_reuse``
     - whether to use the `reuse tool`_ or not. If not, you can ignore the
       ``templates_license``, ``documentation_license`` and
       ``supplementary_files_license``
     - ``yes``
     - ``yes`` or ``no``
   * - ``templates_license``
     - SPDX license identifier for the helm chart templates.
       *Can be ignored if use_reuse is no*.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``"EUPL-1.2"``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``documentation_license``
     - SPDX license identifier for the documentation.
       *Can be ignored if use_reuse is no*.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``"CC-BY-4.0"``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``supplementary_files_license``
     - SPDX license identifier for technical files such as ``.gitignore`` or
       package configuration.
       *Can be ignored if use_reuse is no*.

       .. important::

          Please clarify this with the head of your institute! Changing
          licenses afterwards can be very difficult.

     - ``"CC0-1.0"``
     - A valid SPDX-identifier, see https://spdx.org/licenses/
   * - ``project_title``
     - Short title for the project
     - ``"My First Helm Chart"``
     - string, can contain spaces
   * - ``project_slug``
     - Slug of the project on GitLab
     - Fallback to ``project_title`` but with ``-`` instead of spaces
     - string without spaces or slashes (should match the following pattern
       ``^[-a-zA-Z0-9_]+\Z``)
   * - ``template_base_name``
     - base name for objects that are created with this chart and template
       files
     - ``"{{ cookiecutter.project_slug }}"``
     - string without spaces or slashes (should match the following pattern
       ``^[-a-zA-Z0-9_]+\Z``)
   * - ``project_short_description``
     - Optional description for the package
     - *empty string*
     - arbitrary string
   * - ``keywords``
     - keywords for the project
     - *empty string*
     - comma-separated list of strings. Each string should represent one
       keyword
   * - ``create_templates``
     - Shortcut to avoid the creation of any templates. Use this, if you only
       want to create the helm chart skeleton
       (``Charts.yaml``, ``.gitlab-ci.yaml``, etc.) but do want to use your own
       templates
     - ``yes``
     - ``yes`` or ``no``
   * - ``include_deployment``
     - Create a template for a ``Deployment`` or ``DeploymentConfig``? If you
       choose ``yes`` and want to have a ``BuildConfig``, we will use a
       ``DeploymentConfig`` with an ``ImageStream``, otherwise we will use a
       ``Deployment``. *This option is ignored when create_templates is no*.
     - ``yes``
     - ``yes`` or ``no``
   * - ``include_build_config``
     - Create the template for a ``BuildConfig``. *This option is ignored when
       create_templates is no*.
     - ``yes``
     - ``yes`` or ``no``
   * - ``include_build_test_stage``
     - Include a test stage in the build config.  *This option is ignored when
       create_templates is no*.
     - ``no``
     - ``yes`` or ``no``
   * - ``include_k8sgitlab_secret``
     - Create the template for a ``Secret`` to pull from the ``gitlab_host``.
       This template is used by the ``BuildConfig`` to pull the remote git
       repository. *This option is ignored when create_templates is no*.
     - ``yes``
     - ``yes`` or ``no``
   * - ``include_service``
     - Create the template for a ``Service``. See also the `service_port` and
       `target_service_port` below.
       *This option is ignored when create_templates is no*.
     - ``yes``
     - ``yes`` or ``no``
   * - ``include_route``
     - Create the template for a ``Route`` that attaches to the ``Service``.
       *This option is ignored when create_templates is no*.
     - ``no``
     - ``yes`` or ``no``
   * - ``include_volume``
     - Create templates for one or more ``PersistentVolumeClaims`` (``PVC``).
       PVCs can be configured  through the ``values.yaml`` file.
       *This option is ignored when create_templates is no*.
     - ``no``
     - ``no`` for no PVC, ``one`` for exactly one PVC, ``many`` for
       multiple PVCs
   * - ``include_secret``
     - Create the template for a ``Secret`` that is added to the application
       pod. *This option is ignored when create_templates is no*.
     - ``no``
     - ``yes`` or ``no``
   * - ``include_configmap``
     - Create the template for a ``ConfigMap`` that is added to the
       application pod. *This option is ignored when create_templates is no*.
     - ``no``
     - ``yes`` or ``no``
   * - ``default_deployment_strategy``
     - Default rollout strategy for the deployment. ``recreate`` will shutdown
       any existing pods before starting a new one, ``rolling`` will shutdown
       old containers ones the new ones are up and running.
       *This option is ignored when create_templates or include_deployment is no*.
     - ``rolling``
     - ``rolling`` or ``recreate``
   * - ``service_port``
     - Port that the service should be attached to (requires
       ``include_service``)
     - *empty*
     - An integer
   * - ``helm_registry``
     - Where should the registry be published? You can use a public repository
       (hcdc-public_), an internal one (hcdc-internal_) or specify your own
       package registry (`custom-registry`) via the next parameter,
       `custom_helm_registry_id`
     - ``hcdc-public``
     - ``hcdc-public``, ``hcdc-internal`` or ``custom-registry``
   * - ``custom_helm_registry_id``
     - The Gitlab project ID that you want to use in case you use a
       ``custom-registry`` as `helm_registry`
     - *empty*
     - An integer


.. _reuse tool: https://reuse.readthedocs.io
.. _hcdc-public: https://codebase.helmholtz.cloud/hcdc/kubernetes/charts/public-helm-charts
.. _hcdc-internal: https://codebase.helmholtz.cloud/hcdc/kubernetes/charts/internal-helm-charts