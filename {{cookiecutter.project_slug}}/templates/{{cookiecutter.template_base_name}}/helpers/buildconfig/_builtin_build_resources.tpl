{%- if cookiecutter.__include_build_config == "yes" %}
{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.builtinBuildResources" -}}
limits:
  cpu: '1'
  memory: 1000Mi
requests:
  cpu: 50m
  memory: 50Mi
{{- end }}
{%- endraw %}
{%- endif %}