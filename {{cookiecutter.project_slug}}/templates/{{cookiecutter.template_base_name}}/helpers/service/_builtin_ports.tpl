{%- if cookiecutter.__include_service == "yes" %}
{{ "{{-" }} define "{{ cookiecutter.template_base_name }}.builtinServicePorts" {{ "-}}" }}
default:
  port: {{ cookiecutter.service_port }}
{% raw %}{{- end }}{% endraw %}
{%- endif %}