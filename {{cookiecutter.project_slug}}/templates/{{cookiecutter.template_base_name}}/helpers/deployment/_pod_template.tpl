{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.podTemplate" -}}
{%- endraw %}

{%- if cookiecutter.__include_volume != 'no' %}
{%- raw %}
{{- $builtinVolumes := include "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.builtinVolumes" . | fromYaml }}
{{- $allVolumes := mustMergeOverwrite (dict) $builtinVolumes .Values.volumes }}
{%- endraw %}
{%- endif %}

metadata:
  creationTimestamp: null
  labels:
    {{ "{{-" }} include "{{ cookiecutter.template_base_name }}.selectorLabels" . | nindent 4 {{ "}}" }}
    io.kompose.service: {% raw %}{{ .Values.baseName }}{% endraw %}
    {%- if cookiecutter.include_route == "yes" %}
    access: public
    {%- endif %}
  annotations:
    {%- if cookiecutter.__include_volume != 'no' %}
    {%- raw %}
    {{- $excludedVolumes := dict "volumes" list }}
    {{- range $name, $options := $allVolumes }}
    {{- if or $options.projected $options.excludeFromBackup }}
    {{- $_ := set $excludedVolumes "volumes" (append $excludedVolumes.volumes $name) }}
    {{- end }}
    {{- end }}
    {{- if $excludedVolumes.volumes }}
    backup.velero.io/backup-volumes-excludes: {{ join "," $excludedVolumes.volumes | quote }}
    {{- end }}
    {%- endraw %}
    {%- endif %}

spec:
  affinity: {{ "{{-" }} include "{{ cookiecutter.template_base_name }}.affinity" . | nindent 4 {{ "}}" }}
  {%- raw %}
  {{- if .Values.useManyFilesPVC }}
  securityContext:
    fsGroupChangePolicy: OnRootMismatch
    seLinuxOptions:
      type: spc_t
  serviceAccountName: manyfilespvc
  serviceAccount: manyfilespvc
  imagePullSecrets:
    - name: deployer-quay-{{ .Values.global.openshiftCluster }}
  {{- end }}
  {%- endraw %}
  containers:
    - name: {% raw %}{{ .Values.baseName }}{% endraw %}
      {%- raw %}
      {{- if .Values.command }}
      command:
        {{- range .Values.command }}
        - {{ . | quote }}
        {{- end }}
      {{- end }}
      {%- endraw %}
      {%- if cookiecutter.__include_build_config == "yes" %}
      image: '{% raw %}"{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ .Values.baseName }}"{% endraw %}'
      {%- else %}
      image: {% raw %}"{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ required "A source image (fromImage) for {% endraw %}{{ cookiecutter.template_base_name }}{% raw %} is required!" .Values.fromImage }}"{% endraw %}
      {%- endif %}
      {%- if cookiecutter.include_secret =="yes" or cookiecutter.include_configmap =="yes" %}
      envFrom:
        {%- if cookiecutter.include_secret =="yes" %}
        - secretRef:
            name: {% raw %}{{ .Values.secretName | default .Values.baseName }}{% endraw %}
        {%- endif %}
        {%- if cookiecutter.include_configmap =="yes" %}
        - configMapRef:
            name: {% raw %}{{ .Values.configMapName | default .Values.baseName }}{% endraw %}
        {%- endif %}
      {%- endif %}
      {%- if cookiecutter.service_port %}
      ports:
        - containerPort: {{ cookiecutter.service_port }}
      {%- endif %}
      {%- raw %}
      {{- $builtinResources := include "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.builtinResources" . | fromYaml }}
      {{- $mergedResources := mustMergeOverwrite (dict) $builtinResources .Values.resources }}
      resources: {{ toYaml $mergedResources | nindent 8 }}
      {%- endraw %}
      {%- if cookiecutter.__include_volume != 'no' %}
      volumeMounts:
        {%- raw %}
        {{- range $name, $options := $allVolumes }}
        {{- $defaultMountPath := print "/opt/app-root/src/" $name }}
        - mountPath: {{ $options.mountPath | default $defaultMountPath | quote }}
          name: {{ $name | quote }}
          readOnly: {{ if or (not (hasKey $options "readOnly")) (eq (coalesce $options.readOnly false) true) }}true{{ else }}false{{ end }}
        {{- end }}
        {%- endraw %}
      {%- endif %}
  restartPolicy: Always
  {%- if cookiecutter.__include_volume != 'no' %}
  volumes:
  {%- raw %}
    {{- range $name, $options := $allVolumes }}
    - name: {{ $name | quote }}
      {{- if $options.projected }}
      projected: {{ toYaml $options.projected | nindent 8 }}
      {{- else }}
      persistentVolumeClaim:
        claimName: {{ $name | quote }}
      {{- end }}
    {{- end }}
  {%- endraw %}
  {%- endif %}
{% raw %}{{- end }}{% endraw %}
