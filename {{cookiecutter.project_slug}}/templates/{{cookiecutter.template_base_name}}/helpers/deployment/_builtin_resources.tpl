{%- if cookiecutter.__include_deployment != "no" %}
{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.builtinResources" -}}
limits:
  cpu: 200m
  memory: 200Mi
requests:
  cpu: 50m
  memory: 50Mi
{{- end }}
{%- endraw %}
{%- endif %}