{%- if cookiecutter.__include_deployment != "no" %}

{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.affinity" -}}
{%- endraw %}
{%- if cookiecutter.__include_volume != 'no' %}
{%- raw %}
{{- /* due to scoping we need to use a dict for requiredPodAffinity and override the value if necessary */}}
{{- $requiredPodAffinity := dict "isRequired" false }}
{{- $builtinVolumes := include "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.builtinVolumes" . | fromYaml }}
{{- $allVolumes := mustMergeOverwrite (dict) $builtinVolumes .Values.volumes }}
{{- range $name, $options := $allVolumes }}
  {{- /* Check the necessity of podAffinity */}}
  {{- if and (not $options.projected) (hasKey $options "accessModes") }}
    {{- range $options.accessModes }}
      {{- if eq . "ReadWriteOnce" }}
        {{- $_ := set $requiredPodAffinity "isRequired" true }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{%- endraw %}
{%- endif %}
podAffinity:
  requiredDuringSchedulingIgnoredDuringExecution:
    {%- if cookiecutter.__include_volume != 'no' %}
    {% raw %}{{- if $requiredPodAffinity.isRequired }}{% endraw %}
    - labelSelector:
        matchLabels:
          io.kompose.service: {% raw %}{{ .Values.baseName }}{% endraw %}
          {{ "{{-" }} include "{{ cookiecutter.template_base_name }}.selectorLabels" . | nindent 10 {{ "}}" }}
      topologyKey: kubernetes.io/hostname
    {% raw %}{{- end }}{% endraw %}
    {%- endif %}
podAntiAffinity:
  preferredDuringSchedulingIgnoredDuringExecution:
    {%- if cookiecutter.__include_volume != 'no' %}
    {% raw %}{{- if not $requiredPodAffinity.isRequired }}{% endraw %}
    # to ensure constant availablility of the application, we deploy different
    # pods on different nodes
    - weight: 100
      podAffinityTerm:
        labelSelector:
          matchLabels:
            io.kompose.service: {% raw %}{{ .Values.baseName }}{% endraw %}
            {{ "{{-" }} include "{{ cookiecutter.template_base_name }}.selectorLabels" . | nindent 12 {{ "}}" }}
        topologyKey: kubernetes.io/hostname
    {% raw %}{{- end }}{% endraw %}
    {%- endif %}
{%- raw %}
{{- end }}
{%- endraw %}
{%- endif %}