{%- if cookiecutter.__include_workers != "no" %}
{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.defaultWorkerResources" -}}
limits:
  cpu: "200m"
  memory: "200Mi"
requests:
  cpu: "50m"
  memory: "50Mi"
{{- end }}
{%- endraw %}
{%- endif %}