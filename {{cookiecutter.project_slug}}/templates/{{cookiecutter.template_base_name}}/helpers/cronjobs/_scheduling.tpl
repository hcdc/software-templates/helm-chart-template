{%- if cookiecutter.__include_cronjobs == "yes" %}
{%- raw %}{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.cronJobScheduling" -}}{% endraw %}
schedule: "*/1 * * * *"
concurrencyPolicy: "Replace"
startingDeadlineSeconds: 200
suspend: {% raw %}{{ if .Values.shutdown }}true{{ else }}false{{ end }}{% endraw %}
successfulJobsHistoryLimit: 3
failedJobsHistoryLimit: 1
{% raw %}{{- end }}{%- endraw %}
{%- endif %}
