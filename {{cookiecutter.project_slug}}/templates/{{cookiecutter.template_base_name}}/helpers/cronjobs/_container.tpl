{%- if cookiecutter.__include_cronjobs == "yes" %}
{%- raw %}{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.cronJobContainer" -}}{% endraw %}

{%- if cookiecutter.__include_build_config == "yes" %}
image: {% raw %}"{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ .Values.baseName }}"{% endraw %}
{%- else %}
image: {% raw %}"{{ printf "%s-quay-registry.apps.%s.fzg.local" (regexFind "dev|prod" .Values.global.openshiftCluster) .Values.global.openshiftCluster }}/{{ required "You need to specify a cluster" .Values.global.openshiftCluster }}_{{ .Release.Namespace }}/{{ required "A source image (fromImage) for {% endraw %}{{ cookiecutter.template_base_name }}{% raw %} is required!" .Values.fromImage }}"{% endraw %}
{%- endif %}
{%- if cookiecutter.include_secret =="yes" or cookiecutter.include_configmap =="yes" %}
envFrom:
  {%- if cookiecutter.include_secret =="yes" %}
  - secretRef:
      name: {% raw %}{{ .Values.baseName }}{% endraw %}
  {%- endif %}
  {%- if cookiecutter.include_configmap =="yes" %}
  - configMapRef:
      name: {% raw %}{{ .Values.baseName }}{% endraw %}
  {%- endif %}
{%- endif %}
{% raw %}
command: {{ range (required "Specify a command to run in the cronjob" .command) }}
  - {{ . | quote }}
{{- end }}
{%- endraw %}
{%- if cookiecutter.__include_volume != 'no' %}
volumeMounts:
{%- raw %}
{{- range $name, $options := .volumes }}
{{- $defaultMountPath := print "/opt/app-root/src/" $name }}
  - mountPath: {{ $options.mountPath | default $defaultMountPath | quote }}
    name: {{ $name | quote }}
    readOnly: {{ if or (not (hasKey $options "readOnly")) (eq (coalesce $options.readOnly false) true) }}true{{ else }}false{{ end }}
{{- end }}
{%- endraw %}
{%- endif %}
{%- raw %}
{{- end }}
{%- endraw %}
{%- endif %}
