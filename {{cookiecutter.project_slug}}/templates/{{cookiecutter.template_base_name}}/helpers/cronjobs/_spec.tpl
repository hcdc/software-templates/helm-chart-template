{%- if cookiecutter.__include_cronjobs == "yes" %}
{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.cronJobSpec" -}}
{{- $builtinScheduling := include "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.cronJobScheduling" . | fromYaml }}
{{- $mergedScheduling := mustMergeOverwrite (dict) $builtinScheduling (.scheduling | default (dict)) }}
{{ toYaml $mergedScheduling }}
{%- endraw %}

{%- if cookiecutter.__include_volume != 'no' %}
{%- raw %}
{{- /* due to scoping we need to use a dict for requiredPodAffinity and override the value if necessary */}}
{{- $requiredPodAffinity := dict "isRequired" false }}
{{- if .volumes }}
  {{- range $name, $options := .volumes }}
    {{- /* Check the necessity of podAffinity */}}
    {{- if and (not $options.projected) (hasKey $options "accessModes") }}
      {{- range $options.accessModes }}
        {{- if eq . "ReadWriteOnce" }}
          {{- $_ := set $requiredPodAffinity "isRequired" true }}
        {{- end }}
      {{- end }}
    {{- end }}
  {{- end }}
{{- end }}
{%- endraw %}
{%- endif %}

jobTemplate:
  spec:
    template:
      metadata:
        labels:
          parent: {%raw %}{{ .name | quote }}{% endraw %}
        annotations:
          {%- if cookiecutter.__include_volume != 'no' %}
          {%- raw %}
          {{- $excludedVolumes := dict "volumes" list }}
          {{- range $name, $options := .volumes }}
          {{- if or $options.projected $options.excludeFromBackup }}
          {{- $_ := set $excludedVolumes "volumes" (append $excludedVolumes.volumes $name) }}
          {{- end }}
          {{- end }}
          {{- if $excludedVolumes.volumes }}
          backup.velero.io/backup-volumes-excludes: {{ join "," $excludedVolumes.volumes | quote }}
          {{- end }}
          {%- endraw %}
          {%- endif %}
      spec:
        {%- if cookiecutter.__include_volume != 'no' %}
        affinity:
          podAffinity:
            requiredDuringSchedulingIgnoredDuringExecution:
              {% raw %}{{- if $requiredPodAffinity.isRequired }}{% endraw %}
              - labelSelector:
                  matchLabels:
                    io.kompose.service: {% raw %}{{ $.Values.baseName }}{% endraw %}
                    {{ "{{-" }} include "{{ cookiecutter.template_base_name }}.selectorLabels" $ | nindent 20 {{ "}}" }}
                topologyKey: kubernetes.io/hostname
              {% raw %}{{- end }}{% endraw %}
        {%- endif %}
        {%- raw %}
        {{- if .Values.useManyFilesPVC }}
        securityContext:
          fsGroupChangePolicy: OnRootMismatch
          seLinuxOptions:
            type: spc_t
        serviceAccountName: manyfilespvc
        serviceAccount: manyfilespvc
        imagePullSecrets:
          - name: deployer-quay-{{ .Values.global.openshiftCluster }}
        {{- end }}
        {%- endraw %}
        containers:
          - name: {%raw %}{{ .name | quote }}{% endraw %}
            {%- raw %}
            {{- $builtinContainer := include "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.cronJobContainer" . | fromYaml }}
            {{- $mergedContainer := mustMergeOverwrite (dict) $builtinContainer (.container | default (dict)) }}
            {{- toYaml $mergedContainer | nindent 12 }}
            {%- endraw %}
        {%- if cookiecutter.__include_volume != 'no' %}
        volumes:
        {%- raw %}
        {{- range $name, $options := .volumes }}
          - name: {{ $name | quote }}
            {{- if $options.projected }}
            projected: {{ toYaml $options.projected | nindent 14 }}
            {{- else }}
            persistentVolumeClaim:
              claimName: {{ $name | quote }}
            {{- end }}
        {{- end }}
        {%- endraw %}
        {%- endif %}
        restartPolicy: {% raw %}{{ .restartPolicy | default "OnFailure" | quote }}{% endraw %}
{% raw %}{{- end }}{% endraw %}
{%- endif %}
