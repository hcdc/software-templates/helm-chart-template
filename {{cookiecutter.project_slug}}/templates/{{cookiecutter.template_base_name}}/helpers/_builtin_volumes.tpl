{%- if cookiecutter.__include_volume != "no" %}
{%- raw %}
{{- define "{% endraw %}{{ cookiecutter.template_base_name }}{% raw %}.builtinVolumes" -}}
{%- endraw %}
{%- if cookiecutter.__include_volume == "one" %}
{{ cookiecutter.template_base_name }}:
  storage: "1Gi"
  mountPath: "/opt/app-root/src/{{ cookiecutter.template_base_name }}"
{%- else %}
{}
{%- endif %}
{% raw %}{{- end }}{% endraw %}
{%- endif %}