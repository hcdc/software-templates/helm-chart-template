global:
  # Global Parameters for the gitlab host from where to build the source
  # images
  {%- if cookiecutter.__include_build_config == "yes" %}
  buildSource:
    gitlabHost: {{ cookiecutter.gitlab_host }}
    gitlabProject: null
    # a branch or tag to build from. defaults to the default branch (i.e. main)
    gitlabRef: null
    # the secret with the private key to access the project on gitlab
    gitlabSourceSecret: k8sgitlab
  {%- endif %}
  # cluster where this chart should be deployed
  openshiftCluster: null
  manualDeployKey: false

{%- if cookiecutter.__include_build_config == "yes" or cookiecutter.__include_deployment == "yes" or cookiecutter.__include_cronjobs == "yes" %}

# the origin of the docker image to build
fromImage: null

# buildSource is a mapping with overrides of global.buildSource
buildSource:
  gitlabHost: null
  gitlabProject: null
  gitlabRef: null
  gitlabSourceSecret: null
{%- endif %}

{%- if cookiecutter.__include_deployment == "yes" or cookiecutter.__include_workers == "yes" %}

# command is the command that the docker container shall run (has to be a list)
command: null
{%- endif %}

# shutdown is a boolean flag that suspends all cronjobs and deployments
# of this helm chart. This is a shortcut to the replicas.
shutdown: false

{%- if cookiecutter.__include_build_config == "yes" %}

# secrets is a mapping of secret key to secret default. These values
# can be used to generate a secret that is used during the build.
buildSecrets: {}

# buildSecretName is the name of the secret to use during the build.
buildSecretName: null

# a mapping to specify where to push the image to.
buildTargets: {}
  # <key>: # the name for the secret (can be the namespace to use)
  #   cluster: <the cluster to push to (used to obtain the registry)
  #   namespace: <target k8s namespace>  # optional. when empty, <key> is used

# successfulBuildsHistoryLimit is the number of old successful builds to
# retain. When a BuildConfig is created, the 2 most recent successful builds
# are retained unless this value is set. If removed after the BuildConfig has
# been created, all successful builds are retained.
successfulBuildsHistoryLimit: 2

# failedBuildsHistoryLimit is the number of old failed builds to retain. When a
# BuildConfig is created, the 5 most recent failed builds are retained unless
# this value is set. If removed after the BuildConfig has been created, all
# failed builds are retained.
failedBuildsHistoryLimit: 2

# buildArgs is a mapping from the argument to the value to be used in the
# Dockerfile during the build process
buildArgs: {}

# buildResources are requests and limits for cpu and memory for the pods that
# build the image
buildResources: {}
  # limits:
  #   cpu: '1'
  #   memory: 1000Mi
  # requests:
  #   cpu: 50m
  #   memory: 50Mi

# custom triggers that can be used to trigger a build
buildTriggers: []
# - imageChange: {}
#   type: ImageChange

# directory in the git repository where the Dockerfile is located
contextDir: "docker/{{ cookiecutter.template_base_name }}"

# path to the Dockerfile, relative to the given contextDir. If empty, 
# Dockerfile is used
dockerfilePath: null

{%- if cookiecutter.include_build_test_stage == "yes" %}

# directory in the git repository where the Dockerfile for the test setup is 
# located
testContextDir: "docker/{{ cookiecutter.template_base_name }}/tests"

# path to the Dockerfile for the test stage, relative to the given 
# testContextDir. If empty, Dockerfile is used
testDockerfilePath: null
{%- endif %}

{%- if cookiecutter.include_build_test_stage == "yes" %}

# testStagePostCommit contains the command to run when the image from the
# test stage has been built, see
# https://docs.openshift.com/container-platform/4.10/rest_api/workloads_apis/buildconfig-build-openshift-io-v1.html#spec-postcommit
testStagePostCommit:
{%- endif %}

{%- endif %}

{%- if cookiecutter.__include_service == "yes" %}

# servicePorts is a mapping from port name to port and targetPort. Default
# ports are
#
#     default:
#       port: {{ cookiecutter.service_port }}
#
# If your want to omit a builtin port, set the `port` value to `null`
servicePorts: {}

{%- endif %}

{%- if cookiecutter.__include_route == "yes" %}

routes:
  # the keys of the routes mapping are the names of the route is the name for the
  # route
  {{ cookiecutter.template_base_name }}:
    # port is the port of the service that the route should point at
    port: default
    # serviceName can be the name of the service if you want to use a different
    # service than the one you create with `baseName`
    # serviceName: {{ cookiecutter.template_base_name }}
    # sameSiteCookie can be used to create a cookie for the route for
    # statefulness (see
    # https://docs.openshift.com/container-platform/4.11/networking/routes/route-configuration.html#nw-using-cookies-keep-route-statefulness_route-configuration)
    # sameSiteCookie: Strict
    # host can be a string to create a custom route
    # host: null
    # https can be set to true to create a route with edge termination at the
    # cluster
    https: true
{%- endif %}

# the name for objects that are created with this chart
baseName: {{ cookiecutter.template_base_name }}

{%- if cookiecutter.__include_volume != "no" %}

# volumes is a mapping of volume name to mounting options.
{%- if cookiecutter.__include_volume == "one" %}
# Builtin volumes for this chart (see the `_builtin_volumes.tpl` file) are
#
#   {{ cookiecutter.template_base_name }}:
#     storage: "1Gi"
#     mountPath: "/opt/app-root/src/{{ cookiecutter.template_base_name }}"
#
# You can overwrite any of these settings or define new ones with the `volumes`
# parameter here
{%- endif %}
volumes: {}
  # <some-name>:  # the volume name
  #   mountPath: /opt/app-root/src/<some-name>  # path where to mount in the container, optional
  #   storage: "1Gi"  # optional
  #   keep: true  # optional
  #   readOnly: true  # optional
  #   excludeFromBackup: false  # optional
  #   storageClassName: ""  # optional
  #   projected: {}  # alternative to creating a PVC, see https://docs.openshift.com/container-platform/4.10/nodes/containers/nodes-containers-projected-volumes.html
  #   volumeName: <some-pv>  # the name of a PersistentVolume, e.g. if you want to mount via NFS
  #   mountOnly: false  # optional, if the volume is created somewhere else
  #   accessModes:  # optional, list of access modes within the container. if omitted, we use ReadWriteMany
  #     - "ReadWriteMany"
{%- endif %}

{%- if cookiecutter.__include_deployment != "no" %}
# deploymentStrategy is the strategy used for rolling out new pods by the
# Deployment. Can be either RollingUpdate or Recreate. Recreate will shut down
# the existing deployment before creating a new one.
deploymentStrategy: {% if cookiecutter.default_deployment_strategy == "rolling" %}RollingUpdate{% else %}Recreate{% endif %}

# resources are requests and limits for cpu and memory for this pod
resources: {}
  # limits:
  #   cpu: 1000m
  #   memory: 8000Mi
  # requests:
  #   cpu: 1000m
  #   memory: 4000Mi

# replicas is the number of pod you want your application to scale to
replicas: 1
{%- endif %}

{%- if cookiecutter.__include_secret == "yes" %}

# secrets is a mapping of secret key to secret default. These values are supposed to be implemented by parent charts
secrets: {}
  # SECRET_KEY: "some default value"
{%- endif %}

{%- if cookiecutter.__include_configmap == "yes" %}

# config is a mapping of config key to config value.
config: {}
  # CONFIG_KEY: "some value"
{%- endif %}

{%- if cookiecutter.__include_cronjobs == "yes" %}

# cronJobs is a mapping from cronJob name to cronJob options
cronJobs: {}
  # <some-name>:  # the name for the CronJob item
  #   command: [] # the command to run as a list
  #   restartPolicy: "OnFailure"  # optional
  #   container: {} # additional settings for the container
{%- if cookiecutter.__include_volume != "no" %}
  #   volumes:  # PVCs that should be mounted on the container
  #     <some-name>:  # the name of the PVC
  #       mountPath: /opt/app-root/src/<some-name>  # path where to mount in the container, optional
  #       readOnly: true  # optional
{%- endif %}
  #   scheduling: # additional scheduling info for the container, by default
  #     schedule: "*/1 * * * *"
  #     concurrencyPolicy: "Replace"
  #     startingDeadlineSeconds: 200
  #     suspend: false
  #     successfulJobsHistoryLimit: 3
  #     failedJobsHistoryLimit: 1
{%- endif %}

{%- if cookiecutter.__include_deployment != "no" or cookiecutter.__include_cronjobs == "yes" or cookiecutter.__include_workers == "yes" %}

# useManyFilesPVC is a boolean. If true, we expect that the cluster-admin added
# a `manyfilespvc` serviceaccount. You should use this option when you expect
# that a PVC mounted on the container contains houndreds of thousands of files,
# it uses `seLinuxOptions: {type: spc_t}` in the `securityContext`.
# 
# See https://access.redhat.com/solutions/6221251 for information on how this
# is implemented
useManyFilesPVC: false
{%- endif %}

{%- if cookiecutter.__include_workers == "yes" %}

# workers is a mapping from worker name to container options
workers: {}
  # <some-name>: the name for the Deployment item
  #   command: []  # the command to run as a list
  #   resources:
  #     limits:
  #       cpu: 50m
  #       memory: 50Mi
  #     requests:
  #       cpu: 200m
  #       memory: 200Mi
  {%- if cookiecutter.__include_build_config == "yes" %}
  # deploymentStrategy: {% if cookiecutter.default_deployment_strategy == "rolling" %}Rolling{% else %}Recreate{% endif %} # strategy for rolling out new pods (see deploymentStrategy above)
  {%- else %}
  # deploymentStrategy: {% if cookiecutter.default_deployment_strategy == "rolling" %}RollingUpdate{% else %}Recreate{% endif %}  # strategy for rolling out new pods (see deploymentStrategy above)
  {%- endif %}
  # resources: {} # requests and limits for cpu and memory for this pod
{%- endif %}