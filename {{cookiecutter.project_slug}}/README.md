# {{ cookiecutter.project_slug }} Helm Chart

{% if cookiecutter.project_short_description %}
{{ cookiecutter.project_short_description }}
{% endif %}

## Installation

You need to have [helm](https://helm.sh/docs/intro/install/) installed.

First of all, login to the openshift cluster via

```bash
oc login --token=<token> --server=https://api.<openshift-host>:6443
```

and activate the project you want to work on via

```bash
oc project <project-name>
```

You then need to add the repo that includes this chart via:

```bash
CHANNEL=stable  # or latest
helm repo add {% if cookiecutter.__helm_registry_public == "no" %}--username <username> --password <access_token>{% endif %} {{ cookiecutter.helm_registry }} https://{{ cookiecutter.gitlab_host }}/api/v4/projects//{{ cookiecutter.__helm_registry }}/packages/helm/${CHANNEL}
helm repo update
```

To use this chart, you can now run

```bash
helm install -f your-values.yaml {{ cookiecutter.template_base_name }} {{ cookiecutter.helm_registry }}/{{ cookiecutter.project_slug }}
```

You will then receive instructions how to roll it out. For further options on
customizing this chart with `your-values.yaml`, please run
`helm show values {{ cookiecutter.helm_registry }}/{{ cookiecutter.project_slug }}`
and eventually use the `-f` option instead of `--set` (see the
[limitations of --set](https://helm.sh/docs/intro/using_helm/#the-format-and-limitations-of---set)).

## Updating the chart

To update your deployment, run

```bash
helm repo update  # loads the latest info from the registry
```

and apply the update via

```bash
helm upgrade {{ cookiecutter.project_slug }} {{ cookiecutter.helm_registry }}/{{ cookiecutter.project_slug }}
```


## Contact information

### Authors:
{%- for author_info in get_author_infos(cookiecutter.project_authors.split(','), cookiecutter.project_author_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}

### Maintainers
{%- for author_info in get_author_infos(cookiecutter.project_maintainers.split(','), cookiecutter.project_maintainer_emails.split(',')) %}
- [{{ author_info.full_name }}](mailto:{{ author_info.email }})
{% endfor %}

## License information

Copyright © {{ cookiecutter.copyright_year }} {{ cookiecutter.copyright_holder }}

{% if cookiecutter.use_reuse == "yes" %}
Template files in this repository is licensed under the
{{ cookiecutter.templates_license }}.

Documentation files in this repository is licensed under
{{ cookiecutter.documentation_license }}.

Supplementary and configuration files in this repository are licensed
under {{ cookiecutter.supplementary_files_license }}.

Please check the header of the individual files for more detailed
information.

{% else %}
All rights reserved.
{% endif %}