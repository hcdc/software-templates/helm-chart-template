import git
import os
import subprocess as spr
import glob
import shutil
import sys


{% if cookiecutter.__include_build_config == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/imagestream.yaml")
{% endif %}

{% if cookiecutter.__include_deployment == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/deployment.yaml")
{% if cookiecutter.__include_workers == "no" %}
shutil.rmtree(
    "templates/{{cookiecutter.template_base_name}}/helpers/deployment"
)
{% endif %}
{% endif %}

{% if cookiecutter.__include_workers == "no" %}
os.remove(
    "templates/{{cookiecutter.template_base_name}}/worker-deployments.yaml"
)
shutil.rmtree(
    "templates/{{cookiecutter.template_base_name}}/helpers/workers"
)
{% endif %}

{% if cookiecutter.__include_build_config == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/buildconfig.yaml")
os.remove("templates/{{cookiecutter.template_base_name}}/target-buildconfigs.yaml")
os.remove("templates/{{cookiecutter.template_base_name}}/target-secrets.yaml")
shutil.rmtree(
    "templates/{{cookiecutter.template_base_name}}/helpers/buildconfig"
)
os.remove("templates/{{cookiecutter.template_base_name}}/build-secret.yaml")
os.remove("templates/k8sgitlab-secret.yaml")
{% elif cookiecutter.__include_k8sgitlab_secret == "no" %}
os.remove("templates/k8sgitlab-secret.yaml")
{% endif %}

{% if cookiecutter.__include_cronjobs == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/cronjobs.yaml")
shutil.rmtree(
    "templates/{{cookiecutter.template_base_name}}/helpers/cronjobs"
)
{% endif %}

{% if cookiecutter.__include_service == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/service.yaml")
{% endif %}

{% if cookiecutter.__include_route == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/routes.yaml")
{% endif %}

{% if cookiecutter.__include_volume == "no" %}
os.remove(
    "templates/{{cookiecutter.template_base_name}}/persistentvolumeclaims.yaml"
)
os.remove(
    "templates/{{cookiecutter.template_base_name}}/helpers/_builtin_volumes.tpl"
)
{% endif %}

{% if cookiecutter.__include_secret == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/secret.yaml")
os.remove("templates/{{cookiecutter.template_base_name}}/helpers/_builtin_secrets.tpl")
{% endif %}

{% if cookiecutter.__include_configmap == "no" %}
os.remove("templates/{{cookiecutter.template_base_name}}/configmap.yaml")
os.remove("templates/{{cookiecutter.template_base_name}}/helpers/_builtin_configmap.tpl")
{% endif %}

{% if cookiecutter.use_reuse == "yes" %}

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "docs",
        "--recursive",
        "README.md",
    ]
)

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "supp",
        "--recursive",
        ".gitignore",
        ".pre-commit-config.yaml",
        "Chart.yaml",
        "values.yaml",
        "linter_values.yaml",
        ".gitlab-ci.yml",
    ]
)

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "supp",
        "--recursive",
        "--style",
        "python",
        ".helmignore",
    ]
)

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "template",
        "--recursive",
        "--force-dot-license",
        "templates/NOTES.txt",
    ] + glob.glob("templates/{{cookiecutter.template_base_name}}/*/*.tpl")
    + glob.glob("templates/{{cookiecutter.template_base_name}}/*/*/*.tpl")
)

spr.check_call(
    [
        sys.executable,
        "-m",
        "reuse_shortcuts",
        "template",
        "--recursive",
    ] + glob.glob("templates/*.yaml") + glob.glob("templates/**/*.yaml")
)

spr.check_call(
    [
        "reuse",
        "annotate",
        "--year",
        "{{ cookiecutter.copyright_year }}",
        "--license",
        "Unlicense",
        "--copyright",
        "{{ cookiecutter.copyright_holder }}",
        "scripts/print_version.py",
    ]
)

spr.check_call(["reuse", "download", "--all"])

{% endif %}

print("Fixing files for project at {{cookiecutter.project_slug}}")
repo = git.Repo.init(".", mkdir=False)
{% if cookiecutter.git_remote_protocoll == "ssh" %}
repo.create_remote(
    "origin",
    "git@{{ cookiecutter.gitlab_host }}:{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git",
)
{% else %}
repo.create_remote(
    "origin",
    "https://{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git",
)
{% endif %}
repo.git.add(".")


spr.check_call(["pre-commit", "install"])

# make a silent run first to fix the files
spr.call(["pre-commit", "run"], stdout=spr.DEVNULL)

# add the files again after pre-commit hook has been run
repo.git.add(".")

spr.check_call(["pre-commit", "run"])

for dirpath, dirnames, filenames in os.walk("."):
    if "__pycache__" in dirnames:
        shutil.rmtree(os.path.join(dirpath, "__pycache__"))

print("""
===============================================================================
                          Contratulations!
===============================================================================
You just created a new helm chart for deployments in openshift.

{% if cookiecutter.use_reuse == "yes" -%}
+-------------------------------------------------------------+
| IMPORTANT NOTE:                                             |
|                                                             |
|    If you used cruft to create this package, please now run |
|    the following commands to stage the .cruft.json file and |
|    assign it the correct license:                           |
|                                                             |
+-------------------------------------------------------------+

1. Set the correct license information:

   $ reuse --root {{ cookiecutter.project_slug }} annotate --year {{ cookiecutter.copyright_year }} --license {{ cookiecutter.supplementary_files_license }} --copyright "{{ cookiecutter.copyright_holder }}" {{ cookiecutter.project_slug }}/.cruft.json

2. Stage the file:

   $ git -C {{ cookiecutter.project_slug }} add .cruft.json .cruft.json.license

3. Make your first commit via

   $ git -C {{ cookiecutter.project_slug }} commit -m "Initial commit"

4. Bring your project on the web with

   $ git -C {{ cookiecutter.project_slug }} push -u origin main

3. Allow pushs to the registry from this project. Head over to the registry at

{% else %}
+-------------------------------------------------------------+
| IMPORTANT NOTE:                                             |
|                                                             |
|    You can now push everything to gitlab!                   |
|                                                             |
+-------------------------------------------------------------+

1. Make your first commit via

   $ git -C {{ cookiecutter.project_slug }} commit -m "Initial commit"

2. Bring your project on the web with

   $ git -C {{ cookiecutter.project_slug }} push -u origin main

3. Allow pushs to the registry from this project. Head over to the registry at

{% endif -%}

   https://{{ cookiecutter.gitlab_host }}/projects/{{ cookiecutter.__helm_registry }}

   and go to `Settings` > `CI/CD`.
   Expand the `Token Access` section and allow the CI job token from the
   project

   {{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}


Finally we recommend that you register this package at
https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview
such that the maintainers of the template can support you with keeping your
skeleton up-to-date. You can do this by opening an issue with the following
URL:

https://codebase.helmholtz.cloud/hcdc/software-templates/template-overview/-/issues/new?issue%5Bconfidential%5D=true&issue%5Btitle%5D=New%20template%20usage%20for%20{{ cookiecutter.project_slug }}%20from%20{{ cookiecutter._template.split('/')[-1] }}&issue%5Bdescription%5D=Dear%20HCDC%20support%2C%20I%27d%20like%20to%20register%20the%20following%20repository%0A%0Ahttps%3A//{{ cookiecutter.gitlab_host }}/{{ cookiecutter.gitlab_username }}/{{ cookiecutter.project_slug }}.git%0A%0Athat%20is%20using%20the%20following%20template%0A%0A{{ cookiecutter._template.replace('https:', 'https%3A' )}}"
""")
